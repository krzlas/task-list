package com.sda.tasklist.messages;

import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;

@Component
public class Message {

    private static MessageSource messageSource;

    public Message(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    public static String get(String s, Object[] objects) {
        String outputMessage = messageSource.getMessage(s, objects, null, LocaleContextHolder.getLocale());
        if (outputMessage == null) outputMessage = ":(";
        return outputMessage;
    }
}
