package com.sda.tasklist;


import com.sda.tasklist.repository.todo.ToDoRepository;
import com.sda.tasklist.repository.user.UserRepository;
import com.sda.tasklist.repository.user.UserRoleRepository;
import com.sda.tasklist.model.todo.Status;
import com.sda.tasklist.model.todo.ToDo;
import com.sda.tasklist.model.user.User;
import com.sda.tasklist.model.user.UserRole;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Component
public class MockData {

    private final UserRepository userRepository;
    private final ToDoRepository toDoRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserRoleRepository userRoleRepository;

    public MockData(UserRepository userRepository, ToDoRepository toDoRepository, PasswordEncoder passwordEncoder, UserRoleRepository userRoleRepository) {
        this.userRepository = userRepository;
        this.toDoRepository = toDoRepository;
        this.passwordEncoder = passwordEncoder;
        this.userRoleRepository = userRoleRepository;
    }

    @PostConstruct
    public void mock() {

        if (!userRoleRepository.existsByName("ADMIN")) {
            userRoleRepository.save(new UserRole("ADMIN"));
        }
        if (!userRoleRepository.existsByName("USER")) {
            userRoleRepository.save(new UserRole("USER"));
        }
        if (!userRepository.existsByLogin("mock")) {
            User user = new User();
            user.setLogin("mock");
            user.setPassword(passwordEncoder.encode("mock"));
            user.setEmail("mock@mock.pl");
            user.setBirthday(LocalDate.now());
            user.setSecurityQuestion("mock");
            user.setSecurityAnswer("mock");
            user.getRoles().add(userRoleRepository.findByName("USER"));
            user.getRoles().add(userRoleRepository.findByName("ADMIN"));
            User savedUser = userRepository.save(user);
            ToDo todo = new ToDo();
            todo.setName("Mock Task");
            todo.setDescription("Mock Description");
            todo.setStatus(Status.OPEN);
            todo.setCreationDate(LocalDateTime.now());
            todo.setDeadline(LocalDate.now().plusDays(3));
            todo.setDone(false);
            todo.setUser(savedUser);
            toDoRepository.save(todo);
        }
    }
}
