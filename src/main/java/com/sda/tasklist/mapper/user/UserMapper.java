package com.sda.tasklist.mapper.user;

import com.sda.tasklist.dto.user.CreateUserForm;
import com.sda.tasklist.dto.user.UserDTO;
import com.sda.tasklist.model.user.User;

import java.time.format.DateTimeFormatter;

public class UserMapper {

    private static final String dateTimePattern = "yyyy-MM-dd HH:mm:ss";

    public static User map(CreateUserForm createUserForm) {
        User user = new User();
        user.setLogin(createUserForm.getLogin());
        user.setEmail(createUserForm.getEmail());
        user.setBirthday(createUserForm.getBirthday());
        user.setSecurityQuestion(createUserForm.getSecurityQuestion());
        user.setSecurityAnswer(createUserForm.getSecurityAnswer());
        return user;
    }

    public static UserDTO map(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setLogin(user.getLogin());
        userDTO.setEmail(user.getEmail());
        userDTO.setBirthDate(user.getBirthday());
        userDTO.setLastLoginStamp(user.getLastLoginStamp().format(DateTimeFormatter.ofPattern(dateTimePattern)));
        return userDTO;
    }

}
