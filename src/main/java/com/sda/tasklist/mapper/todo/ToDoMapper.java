package com.sda.tasklist.mapper.todo;

import com.sda.tasklist.dto.todo.CreateToDoForm;
import com.sda.tasklist.dto.todo.ToDoDTO;
import com.sda.tasklist.model.todo.ToDo;

import java.time.format.DateTimeFormatter;

public class ToDoMapper {

    private static final String dateTimePattern = "yyyy-MM-dd HH:mm:ss";
    private static final String datePattern = "yyyy-MM-dd";

    public static ToDoDTO map(ToDo toDo) {
        ToDoDTO toDoDTO = new ToDoDTO();
        toDoDTO.setId(toDo.getId());
        toDoDTO.setName(toDo.getName());
        toDoDTO.setDescription(toDo.getDescription());
        toDoDTO.setStatus(toDo.getStatus());
        toDoDTO.setCreationDate(toDo.getCreationDate().format(DateTimeFormatter.ofPattern(dateTimePattern)));
        toDoDTO.setDeadline(toDo.getDeadline().format(DateTimeFormatter.ofPattern(datePattern)));
        toDoDTO.setDone(toDo.isDone());
        return toDoDTO;
    }

    public static ToDo map(CreateToDoForm createToDoForm){
        ToDo toDo = new ToDo();
        toDo.setName(createToDoForm.getName());
        toDo.setDescription(createToDoForm.getDescription());
        toDo.setDeadline(createToDoForm.getDeadline());
        return toDo;


    }
}
