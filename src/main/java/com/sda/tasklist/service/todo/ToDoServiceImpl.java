package com.sda.tasklist.service.todo;

import com.sda.tasklist.repository.todo.ToDoRepository;
import com.sda.tasklist.repository.user.UserRepository;
import com.sda.tasklist.dto.todo.CreateToDoForm;
import com.sda.tasklist.dto.todo.ToDoDTO;
import com.sda.tasklist.exception.ToDoNotExistsException;
import com.sda.tasklist.mapper.todo.ToDoMapper;
import com.sda.tasklist.model.todo.Sorting;
import com.sda.tasklist.model.todo.Status;
import com.sda.tasklist.model.todo.ToDo;
import com.sda.tasklist.model.user.User;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ToDoServiceImpl implements ToDoService {

    private final ToDoRepository toDoRepository;
    private final UserRepository userRepository;

    private Sorting sorting = Sorting.DEFAULT;

    public ToDoServiceImpl(ToDoRepository toDoRepository, UserRepository userRepository) {
        this.toDoRepository = toDoRepository;
        this.userRepository = userRepository;
    }

    @Override
    public List<ToDoDTO> getToDos(Sorting sort) {
        User user = userRepository.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName()).get();
        if (Arrays.asList(Sorting.values()).contains(sort) && sort != Sorting.DEFAULT) {
            sorting = sort;
        }
        switch (sorting) {
            case DEADLINE:
                return user.getToDos().stream().sorted(Comparator.comparing(ToDo::getDeadline)).map(t -> ToDoMapper.map(t)).collect(Collectors.toList());
            case ID:
            default:
                return user.getToDos().stream().map(t -> ToDoMapper.map(t)).collect(Collectors.toList());
        }
    }

    @Override
    public void addToDo(CreateToDoForm createToDoForm) {
        User user = userRepository.findByLogin(SecurityContextHolder.getContext().getAuthentication().getName()).get();
        ToDo doEntity = ToDoMapper.map(createToDoForm);
        doEntity.setUser(user);
        doEntity.setCreationDate(LocalDateTime.now());
        doEntity.setStatus(Status.OPEN);
        toDoRepository.save(doEntity);
    }

    @Override
    public String getUserLogin() {
        return SecurityContextHolder.getContext().getAuthentication().getName();
    }

    @Override
    public void deleteById(Long id) {
        toDoRepository.deleteById(id);
    }

    @Override
    public ToDoDTO findById(Long id) throws ToDoNotExistsException {
        return ToDoMapper
                .map(toDoRepository
                        .findById(id)
                        .orElseThrow(() -> new ToDoNotExistsException("Task with given id doesn't exist")));
    }

    @Override
    public void editToDo(ToDoDTO toDoDTO) throws ToDoNotExistsException {
        ToDo toDo = toDoRepository
                .findById(toDoDTO.getId())
                .orElseThrow(() -> new ToDoNotExistsException("Task with given id doesn't exist"));
        toDo.setName(toDoDTO.getName());
        toDo.setDescription(toDoDTO.getDescription());
        toDo.setStatus(toDoDTO.getStatus());
        toDo.setDeadline(LocalDate.parse(toDoDTO.getDeadline()));
        toDo.setDone(toDo.getStatus() == Status.CLOSE);
        toDoRepository.save(toDo);
    }

    @Override
    public List<ToDoDTO> getAllToDos() {
        return toDoRepository.findAll().stream().map(t -> ToDoMapper.map(t)).collect(Collectors.toList());
    }

    @Override
    public void markDone(Long id) throws ToDoNotExistsException {
        ToDo toDo = toDoRepository.findById(id).orElseThrow(() -> new ToDoNotExistsException("Task with given id doesn't exist"));
        toDo.setDone(true);
        toDo.setStatus(Status.CLOSE);
        toDoRepository.save(toDo);
    }

    @Override
    public void markUnDone(Long id) throws ToDoNotExistsException {
        ToDo toDo = toDoRepository.findById(id).orElseThrow(() -> new ToDoNotExistsException("Task with given id doesn't exist"));
        toDo.setDone(false);
        toDo.setStatus(Status.REOPEN);
        toDoRepository.save(toDo);
    }

    @Override
    public Long getQuantity() {
        return Long.valueOf(getToDos(Sorting.DEFAULT).size());
    }
}
