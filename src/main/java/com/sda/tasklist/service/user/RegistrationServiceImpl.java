package com.sda.tasklist.service.user;

import com.sda.tasklist.messages.Message;
import com.sda.tasklist.repository.user.UserRepository;
import com.sda.tasklist.repository.user.UserRoleRepository;
import com.sda.tasklist.dto.user.CreateUserForm;
import com.sda.tasklist.exception.UserExistsException;
import com.sda.tasklist.mapper.user.UserMapper;
import com.sda.tasklist.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.LocaleResolver;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Locale;

@Service
public class RegistrationServiceImpl implements RegistrationService {

    private UserRepository userRepository;
    private PasswordEncoder passwordEncoder;
    private UserRoleRepository userRoleRepository;

    public RegistrationServiceImpl(UserRepository userRepository,
                                   PasswordEncoder passwordEncoder,
                                   UserRoleRepository userRoleRepository) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.userRoleRepository = userRoleRepository;
    }

    @Override
    public void addNewUser(CreateUserForm createUserForm) throws UserExistsException {
        if (userRepository.existsByLogin(createUserForm.getLogin())) {
            throw new UserExistsException(Message.get("exception.userExists", Arrays.asList(createUserForm.getLogin()).toArray()));
        }
        if (userRepository.existsByEmail(createUserForm.getEmail())) {
            throw new UserExistsException("User with given Email already exists!");
        }
        User user = UserMapper.map(createUserForm);
        user.setPassword(passwordEncoder.encode(createUserForm.getPassword()));
        user.setLastLoginStamp(LocalDateTime.now());

        if (user.getLogin().contains("admin")) {
            user.getRoles().add(userRoleRepository.findByName("ADMIN"));
        } else {
            user.getRoles().add(userRoleRepository.findByName("USER"));
        }

        userRepository.save(user);
    }
}
