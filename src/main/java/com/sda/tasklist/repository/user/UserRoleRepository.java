package com.sda.tasklist.repository.user;

import com.sda.tasklist.model.user.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

    boolean existsByName(String name);

    UserRole findByName (String name);
}
