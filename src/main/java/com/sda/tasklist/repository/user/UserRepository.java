package com.sda.tasklist.repository.user;

import com.sda.tasklist.model.user.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Optional;


public interface UserRepository extends JpaRepository<User,Long> {

    Optional<User> findByLogin(String login);

    Boolean existsByLogin(String login);

    Boolean existsByEmail(String email);

    @Query("from User u left join fetch u.roles where u.login = :login")
    Optional<User> findUserWithRolesByLogin(String login);

}
