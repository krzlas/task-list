package com.sda.tasklist.repository.todo;

import com.sda.tasklist.model.todo.ToDo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ToDoRepository extends JpaRepository<ToDo, Long> {

}
